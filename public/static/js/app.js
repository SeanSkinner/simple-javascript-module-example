import fetchPosts from "./api/posts.js"
import postsView from "./posts/postsView.js"

const postsContainerElement = document.getElementById("postsContainer")
const filterButtonElement = document.getElementById("filter")
const userIdElement = document.getElementById("userIdInput")

const initialPosts = await fetchPosts()
postsView.initializePosts(initialPosts)

renderActivePosts()

function renderActivePosts() {
    postsContainerElement.innerHTML = ""

    const userIdElementText = userIdElement.value.trim()

    if (userIdElementText !== "") {
        postsView.filterPostsByUserId(Number(userIdElementText))
    }
    
    const activePosts = postsView.getPosts()    

    for (const activePost of activePosts) {
        const newPostElement = document.createElement("p")
        newPostElement.innerText = activePost.title

        postsContainerElement.append(newPostElement)
    } 
}

filterButtonElement.addEventListener("click", () => {
    renderActivePosts()
})