let allPosts = []
let currentPosts = []

const getPosts = () => [...currentPosts]

const initializePosts = (posts) => {
    allPosts = posts
    currentPosts = posts
}

const filterPostsByUserId = (userId) => {
    currentPosts = allPosts.filter(p => p.userId === userId)
}

const postsView = {
    getPosts,
    initializePosts,
    filterPostsByUserId
}

export default postsView
